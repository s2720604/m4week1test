package nl.utwente.di.bookQuote;

import java.util.HashMap;
import java.util.Map;

public class Quoter {

    Map<String, Double> prices = new HashMap<>() {
        {
            put("1", 10.0);
            put("2", 45.0);
            put("3", 20.0);
            put("4", 35.0);
            put("5", 50.0);
        }
    };
    public double getBookPrice(String s) {
        if (prices.containsKey(s)) {
            return prices.get(s);
        }
        return 0;
    }
}
